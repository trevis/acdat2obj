﻿using ACDat2Obj.Lib;
using ACE.DatLoader;
using ACE.DatLoader.Entity;
using ACE.DatLoader.FileTypes;
using CommandLine;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using Environment = ACE.DatLoader.FileTypes.Environment;

namespace ACDat2Obj {
    class Program {
        public class Options {
            [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
            public bool Verbose { get; set; }
            
            [Option('u', "usage", Required = false, HelpText = "Show example usage")]
            public bool Usage { get; set; }

            [Option('d', "dat-dir", Default = @"C:\Turbine\Asheron's Call\", HelpText = "Directory containing dat files")]
            public string DatDirectory { get; set; }

            [Option('o', "output-dir", Default = @"out\", HelpText = "Set output directory")]
            public string OutputDirectory { get; set; }

            [Option('p', "physics-polys", Default = false, HelpText = "Draw physics polygons instead")]
            public bool DrawPhysics { get; set; }

            [Option('f', "flip-yz-axes", Default = false, HelpText = "Flip the y/z axes so y points up")]
            public bool FlipYZAxes { get; set; }

            [Value(0, MetaName = "cellFileIndex", HelpText = "Cell file index to convert")]
            public string CellFileIndex { get; set; }
        }

        public static bool Verbose { get; private set; }
        public static bool DrawPhysics { get; private set; }
        public static ObjWriter ObjWriter { get; private set; }
        public static CellDatDatabase CellDat { get; private set; }
        public static CellDatDatabase PortalDat { get; private set; }

        public static List<List<Vector3>> Polygons { get; private set; } = new List<List<Vector3>>();

        static void Main(string[] args) {
            //* // for testing...
            ObjWriter = new ObjWriter(true);
            LoadDatFiles(@"C:\Turbine\Asheron's Call\");

            LoadLandblockInfo(0x16cfffe, new List<Frame>());
            //*/

            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed<Options>(o => {
                       Verbose = o.Verbose;
                       DrawPhysics = o.DrawPhysics;

                       ObjWriter = new ObjWriter(o.FlipYZAxes);

                       if (o.Usage || string.IsNullOrEmpty(o.CellFileIndex)) {
                           ShowUsage();
                           return;
                       }

                       LoadDatFiles(o.DatDirectory);

                       uint fileIndex = 0;

                       if (o.CellFileIndex.StartsWith("0x")) {
                           o.CellFileIndex = o.CellFileIndex.Replace("0x", "");
                       }

                       if (!uint.TryParse(o.CellFileIndex, System.Globalization.NumberStyles.HexNumber, null, out fileIndex)) {
                           Console.Write($"Unable to parse file index as hex string: {o.CellFileIndex}");
                           ShowUsage();
                           return;
                       }

                       string type = "Unknown";

                       //landblockinfo
                       if (o.CellFileIndex.ToLower().EndsWith("fffe")) {
                           Log($"Found landblockInfo: {fileIndex:X8}");
                           type = "LandblockInfo";
                           LoadLandblockInfo(fileIndex, new List<Frame>());
                       }
                       // gfxObj
                       else if (o.CellFileIndex.ToLower().StartsWith("0100")) {
                           Log($"Found gfxObj: {fileIndex:X8}");
                           type = "GfxObj";
                           LoadGfxObj(fileIndex, new List<Frame>());
                       }
                       // setup
                       else if (o.CellFileIndex.ToLower().StartsWith("0200")) {
                           Log($"Found setup: {fileIndex:X8}");
                           type = "SetupModel";
                           LoadSetup(fileIndex, new List<Frame>());

                       }
                       // environment
                       else if (o.CellFileIndex.ToLower().StartsWith("0d00")) {
                           Log($"Found environment: {fileIndex:X8}");
                           type = "Environment";
                           LoadEnvironment(fileIndex, new List<Frame>());
                       }
                       else {
                           Console.WriteLine($"Error: I don't know how to process that file index: {fileIndex:X8}\n");
                           ShowUsage();
                           return;
                       }

                       if (ObjWriter.Polygons.Count == 0) {
                           Console.WriteLine("Nothing to draw!");
                       }
                       else {
                           var outFile = Path.Combine(o.OutputDirectory, $"{fileIndex:X8}.obj");
                           Console.WriteLine($"Saving {type} {fileIndex:X8} with {ObjWriter.Polygons.Count} polygons and {ObjWriter.Vertices.Count} vertices to file {outFile}");

                           if (!Directory.Exists(o.OutputDirectory))
                               Directory.CreateDirectory(o.OutputDirectory);

                           File.WriteAllText(outFile, ObjWriter.ToObjString());
                       }
                   });
        }

        private static void LoadLandblockInfo(uint cellFileIndex, List<Frame> frames) {
            Log($"Loading landblockInfo: {cellFileIndex:X8}");
            var sw = new Stopwatch();
            sw.Start();
            var landblockInfo = CellDat.ReadFromDat<LandblockInfo>(cellFileIndex);

            for (var i = 0; i < landblockInfo.NumCells; i++) {
                EnvCell cell = CellDat.ReadFromDat<EnvCell>((uint)((landblockInfo.Id >> 16 << 16) + 0x00000100 + i));
                var tempFrames = frames.ToList();
                tempFrames.Add(cell.Position);

                LoadEnvironment(cell.EnvironmentId, tempFrames, cell.CellStructure);

                if (cell.StaticObjects != null && cell.StaticObjects.Count > 0) {
                    foreach (var staticObject in cell.StaticObjects) {
                        var sFrames = frames.ToList();
                        sFrames.Add(staticObject.Frame);
                        Log($"Load staticObject: {staticObject.Id:X8} at {staticObject.Frame}");
                        if ((staticObject.Id & 0x02000000) != 0) {
                            LoadSetup(staticObject.Id, sFrames);
                        }
                        else {
                            LoadGfxObj(staticObject.Id, sFrames);
                        }
                    }
                }
            }

            sw.Stop();
            Log($"Took {sw.ElapsedTicks / 10000f:N4} ms to load landblockInfo {landblockInfo.Id:X8} with {landblockInfo.NumCells} envCells");
        }

        private static void LoadGfxObj(uint cellFileIndex, List<Frame> frames) {
            Log($"Loading gfxObj: {cellFileIndex:X8}");
            var sw = new Stopwatch();
            sw.Start();
            var gfxObj = PortalDat.ReadFromDat<GfxObj>(cellFileIndex);
            var polys = DrawPhysics ? gfxObj.PhysicsPolygons : gfxObj.Polygons;

            // i dont know why, but seems like we should skip BPOL type
            // looking at 0x02000001 human setup model BPOL is extra weird triangles
            if (gfxObj.DrawingBSP.RootNode.Type != "BPOL") {
                ObjWriter.SetTransform(frames);
                foreach (var pkv in polys) {
                    ObjWriter.AddPolygon(pkv.Value.VertexIds.Select(v => gfxObj.VertexArray.Vertices[(ushort)v].Origin).ToList());
                }
                ObjWriter.ClearTransform();
            }

            sw.Stop();
            Log($"Took {sw.ElapsedTicks / 10000f:N4} ms to load gfxObj {gfxObj.Id:X8}");
        }

        private static void LoadSetup(uint cellFileIndex, List<Frame> frames) {
            Log($"Loading setup: {cellFileIndex:X8}");
            var sw = new Stopwatch();
            sw.Start();
            var setupModel = PortalDat.ReadFromDat<SetupModel>(cellFileIndex);

            if (DrawPhysics) {
                // physics draw priority is child part physics polys, then cylspheres, then spheres?
                var hasPhysicsPolys = false;
                foreach (var partId in setupModel.Parts) {
                    var gfxObj = PortalDat.ReadFromDat<GfxObj>(partId);
                    if (gfxObj.PhysicsPolygons != null && gfxObj.PhysicsPolygons.Count > 0) {
                        hasPhysicsPolys = true;
                        break;
                    }
                }

                if (!hasPhysicsPolys && setupModel.CylSpheres != null && setupModel.CylSpheres.Count > 0) {
                    foreach (var cSphere in setupModel.CylSpheres) {
                        List<List<Vector3>> polys = GeometryHelper.GetCylSpherePolygons(cSphere.Origin, cSphere.Height, cSphere.Radius);
                        foreach (var poly in polys) {
                            ObjWriter.SetTransform(frames);
                            ObjWriter.AddPolygon(poly);
                            ObjWriter.ClearTransform();
                        }
                    }
                }
                else if (!hasPhysicsPolys && setupModel.Spheres != null && setupModel.Spheres.Count > 0) {
                    foreach (var sphere in setupModel.Spheres) {
                        List<List<Vector3>> polys = GeometryHelper.GetSpherePolygons(sphere.Origin, sphere.Radius);
                        foreach (var poly in polys) {
                            ObjWriter.SetTransform(frames);
                            ObjWriter.AddPolygon(poly);
                            ObjWriter.ClearTransform();
                        }
                    }
                }
            }

            // draw all the child parts
            for (var i=0; i < setupModel.Parts.Count; i++) {
                // always use PlacementFrames[0] ?
                var tempFrames = frames.ToList(); // clone
                tempFrames.Insert(0, setupModel.PlacementFrames[0].AnimFrame.Frames[i]);
                LoadGfxObj(setupModel.Parts[i], tempFrames);
            }

            sw.Stop();
            Log($"Took {sw.ElapsedTicks / 10000f:N4} ms to load setupModel {setupModel.Id:X8} with {setupModel.Parts.Count} child parts");
        }

        private static void LoadEnvironment(uint cellFileIndex, List<Frame> frames, int envCellIndex=-1) {
            Log($"Loading environment: {cellFileIndex:X8}");
            var sw = new Stopwatch();
            sw.Start();
            var environment = PortalDat.ReadFromDat<Environment>(cellFileIndex);

            if (envCellIndex >= 0) {
                //var polys = DrawPhysics ? environment.Cells[(uint)envCellIndex].PhysicsPolygons : environment.Cells[(uint)envCellIndex].Polygons;
                var polys = environment.Cells[(uint)envCellIndex].PhysicsPolygons;
                ObjWriter.SetTransform(frames);
                foreach (var poly in polys.Values) {
                    ObjWriter.AddPolygon(poly.VertexIds.Select(v => environment.Cells[(uint)envCellIndex].VertexArray.Vertices[(ushort)v].Origin).ToList());
                }
                ObjWriter.ClearTransform();
            }
            else {
                foreach (var envCell in environment.Cells.Values) {
                    var polys = DrawPhysics ? envCell.PhysicsPolygons : envCell.Polygons;
                    ObjWriter.SetTransform(frames);
                    foreach (var poly in polys.Values) {
                        ObjWriter.AddPolygon(poly.VertexIds.Select(v => envCell.VertexArray.Vertices[(ushort)v].Origin).ToList());
                    }
                    ObjWriter.ClearTransform();
                }
            }

            sw.Stop();
            Log($"Took {sw.ElapsedTicks / 10000f:N4} ms to load environment {environment.Id:X8} with {environment.Cells.Count} envCells");
        }

        private static void LoadDatFiles(string datDirectory) {
            Log($"Loading dat files from: {datDirectory}");

            var cellDatPath = Path.Combine(datDirectory, "client_cell_1.dat");
            var portalDatPath = Path.Combine(datDirectory, "client_portal.dat");

            if (!File.Exists(cellDatPath)) {
                Console.WriteLine($"Unable to find cell dat: {cellDatPath}");
                return;
            }

            if (!File.Exists(portalDatPath)) {
                Console.WriteLine($"Unable to find portal dat: {portalDatPath}");
                return;
            }

            var csw = new Stopwatch();
            csw.Start();
            Log($"Loading cell dat from: {cellDatPath}");
            CellDat = new CellDatDatabase(cellDatPath, true);
            csw.Stop();
            Log($"Took {csw.ElapsedMilliseconds / 1000f:N3} seconds to load cell dat");

            var psw = new Stopwatch();
            psw.Start();
            Log($"Loading portal dat from: {portalDatPath}");
            PortalDat = new CellDatDatabase(portalDatPath, true);
            psw.Stop();
            Log($"Took {psw.ElapsedMilliseconds / 1000f:N3} seconds to load portal dat");
        }

        private static void ShowUsage() {
            Console.WriteLine("Converts AC Dat models to obj file format. Currently supports indoor landblocks (0xFFFE w/ numCells>0), environments (0x0D), setup models (0x02), and gfxObj (0x01).");
            Console.WriteLine("");
            Console.WriteLine("Example Usage:");
            Console.WriteLine("# dumps marketplace to obj file in out/");
            Console.WriteLine("\t./ACDat2Obj -o out/ -d mydats/ 0x016cfffe");
            Console.WriteLine("# dumps cow setup model to obj file in out/");
            Console.WriteLine("\t./ACDat2Obj -o out/ -d mydats/ 0x02000006 ");
        }

        private static void Log(string message) {
            if (Verbose) Console.WriteLine(message);
        }
    }
}
