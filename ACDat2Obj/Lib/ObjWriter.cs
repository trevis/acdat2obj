﻿using ACE.DatLoader.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace ACDat2Obj.Lib {
    class ObjWriter {
        public List<Vector3> Vertices { get; } = new List<Vector3>();
        public List<List<int>> Polygons { get; } = new List<List<int>>();

        private List<Frame> frames;
        private bool flipYZAxes;

        public ObjWriter(bool flipYZAxes) {
            this.flipYZAxes = flipYZAxes;
        }

        public void AddPolygon(List<Vector3> vertices) {
            var poly = new List<int>();
            for (var i = 0; i < vertices.Count; i++) {
                var vertice = vertices[i];

                foreach (var frame in frames) {
                    vertice = GeometryHelper.Transform(vertice, frame.Orientation);
                    vertice += frame.Origin;
                }

                if (flipYZAxes) {
                    vertice = new Vector3() {
                        X = -vertice.X,
                        Y = vertice.Z,
                        Z = vertice.Y
                    };
                }

                var index = Vertices.IndexOf(vertice);
                if (index != -1) {
                    // obj face indexes start at 1
                    poly.Add(index + 1);
                }
                else {
                    Vertices.Add(vertice);
                    poly.Add(Vertices.Count);
                }
            }
            Polygons.Add(poly);
        }

        internal void SetTransform(List<Frame> frames) {
            this.frames = frames.ToList();
        }

        internal void ClearTransform() {
            frames.Clear();
        }

        public string ToObjString() {
            var outStr = new StringBuilder();

            foreach (var vertex in Vertices) {
                outStr.Append($"v {vertex.X} {vertex.Y} {vertex.Z}\n");
            }

            foreach (var polygon in Polygons) {
                outStr.Append("f ");
                foreach (var indice in polygon) {
                    outStr.Append($"{indice} ");
                }
                outStr.Append("\n");
            }

            return outStr.ToString();
        }
    }
}
