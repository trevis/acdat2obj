﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ACDat2Obj.Lib {
    public class GeometryHelper {
        public static List<List<Vector3>> GetCylSpherePolygons(Vector3 origin, float height, float radius) {
            return new List<List<Vector3>>();
        }

        public static List<List<Vector3>> GetSpherePolygons(Vector3 origin, float radius) {
            return new List<List<Vector3>>();
        }

        public static Vector3 Transform(Vector3 value, Quaternion rotation) {
            float x2 = rotation.X + rotation.X;
            float y2 = rotation.Y + rotation.Y;
            float z2 = rotation.Z + rotation.Z;

            float wx2 = rotation.W * x2;
            float wy2 = rotation.W * y2;
            float wz2 = rotation.W * z2;
            float xx2 = rotation.X * x2;
            float xy2 = rotation.X * y2;
            float xz2 = rotation.X * z2;
            float yy2 = rotation.Y * y2;
            float yz2 = rotation.Y * z2;
            float zz2 = rotation.Z * z2;

            return new Vector3(
                value.X * (1.0f - yy2 - zz2) + value.Y * (xy2 - wz2) + value.Z * (xz2 + wy2),
                value.X * (xy2 + wz2) + value.Y * (1.0f - xx2 - zz2) + value.Z * (yz2 - wx2),
                value.X * (xz2 - wy2) + value.Y * (yz2 + wx2) + value.Z * (1.0f - xx2 - yy2));
        }
    }
}
