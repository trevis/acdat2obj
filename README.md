# ACDat2Obj

Exports models from Asheron's Call dat files in simple obj format (right now only faces).

## Description

Currently supports indoor landblocks (0xFFFE w/ numCells>0), environments (0x0D), setup models (0x02), and gfxObj (0x01).

## Getting Started

### Installing

* Go to [tagged releases](https://gitlab.com/trevis/acdat2obj/-/tags) and grab latest build.

### Command Line Options
```
  -v, --verbose             Set output to verbose messages.

  -u, --usage               Show example usage

  -d, --dat-dir             (Default: C:\Turbine\Asheron's Call\) Directory containing dat files

  -o, --output-dir          (Default: out\) Set output directory

  -p, --physics-polys       (Default: false) Draw physics polygons instead

  -f, --flip-yz-axes        (Default: false) Flip the y/z axes so y points up

  --help                    Display this help screen.

  --version                 Display version information.

  cellFileIndex (pos. 0)    Cell file index to convert
```

### Example Usage
```
# dumps marketplace to obj file in out/
        ./ACDat2Obj -o out/ -d mydats/ 0x016cfffe
# dumps cow setup model to obj file in out/
        ./ACDat2Obj -o out/ -d mydats/ 0x02000006
```

## TODO

* EnvCell export?
* UV Texture Mapping / Normals